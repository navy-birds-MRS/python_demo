#!/usr/bin/python3
from pytube import YouTube
link = input("Enter the link: ")
yt = YouTube(link)
#Title of video
print("Title: ",yt.title)
#Number of views of video
print("Number of views: ",yt.views)
#Length of the video
print("Length of video: ",yt.length,"seconds")
#Rating
print("Ratings: ",yt.rating)
#printing all the available streams
print(yt.streams)
ys = yt.streams.get_by_itag('140')
ys.download()
